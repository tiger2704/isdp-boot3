package edu.scau.mis.pos.domain.service;

import edu.scau.mis.pos.domain.entity.Category;

import java.util.List;

public interface ICategoryService {
    /**
     * 根据ID查询产品分类
     * @param categoryId
     * @return
     */
    Category selectCategoryById(Long categoryId);

    /**
     * 查询所有产品分类
     * @return
     */
    List<Category> selectAllCategoryList();
}
