package edu.scau.mis.pos.domain.service.impl;

import cn.hutool.core.util.IdUtil;
import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.pos.domain.entity.Payment;
import edu.scau.mis.pos.domain.entity.Sale;
import edu.scau.mis.pos.domain.enums.PaymentStatusEnum;
import edu.scau.mis.pos.domain.enums.PaymentStrategyEnum;
import edu.scau.mis.pos.domain.service.ISaleService;
import edu.scau.mis.pos.infrastructure.adapter.IPayAdapter;
import edu.scau.mis.pos.infrastructure.adapter.factory.PayAdapterFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 领域服务
 */
@Service
@RequiredArgsConstructor
public class SaleServiceImpl implements ISaleService {
    private final PayAdapterFactory payAdapterFactory;

    @Override
    public Payment makePayment(Sale sale, String paymentStrategy, BigDecimal paymentAmount) {
        Payment payment = new Payment();
        payment.setPaymentStrategy(PaymentStrategyEnum.valueOf(paymentStrategy));
        payment.setPaymentNo(paymentStrategy + "-" + IdUtil.getSnowflakeNextId());
        payment.setPaymentAmount(paymentAmount);
        payment.setPaymentTime(new Date());
        payment.setPaymentStatus(PaymentStatusEnum.PAID);

        String payType = payment.getPaymentStrategy().getValue();
        IPayAdapter payAdapter = payAdapterFactory.getPayStrategyAdapterHandler(payType);
        PayRequest payRequest = new PayRequest();
        payRequest.setTradeNo(sale.getSaleNo());
        payRequest.setTotalAmount(paymentAmount);
        payRequest.setSubjectDescription("ISDP项目pos系统移动支付测试");
        payAdapter.pay(payRequest);
        return payment;
    }

}
