package edu.scau.mis.pos.domain.enums;

/**
 * 订单状态枚举
 */
public enum SaleStatusEnum {
    CREATED("0","已预订"),

    SUBMITTED("1","已提交"),

    PAID("2","已支付");

    private String value;

    private String label;

    SaleStatusEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    /**
     * 根据匹配value的值获取Label
     *
     * @param value
     * @return
     */
    public static String getLabelByValue(String value){
        for (SaleStatusEnum s : SaleStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s.getLabel();
            }
        }
        return "";
    }

    /**
     * 获取StatusEnum
     *
     * @param value
     * @return
     */
    public static SaleStatusEnum getStatusEnum(String value){
        for (SaleStatusEnum s : SaleStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s;
            }
        }
        return null;
    }
}
