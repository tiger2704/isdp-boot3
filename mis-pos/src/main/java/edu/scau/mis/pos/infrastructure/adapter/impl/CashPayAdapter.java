package edu.scau.mis.pos.infrastructure.adapter.impl;

import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;
import edu.scau.mis.pos.infrastructure.adapter.IPayAdapter;
import edu.scau.mis.pos.infrastructure.annotation.PaymentStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@PaymentStrategy("cash")
@Slf4j
public class CashPayAdapter implements IPayAdapter {
    @Override
    public PayResponse pay(PayRequest payRequest) {
        log.info("===> 现金支付适配器被调用");
        PayResponse payResponse = new PayResponse();
        payResponse.setTradeNo(payRequest.getTradeNo());
        payResponse.setResponseBody("现金支付");
        return payResponse;
    }
}
