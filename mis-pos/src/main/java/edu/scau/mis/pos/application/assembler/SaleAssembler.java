package edu.scau.mis.pos.application.assembler;

import edu.scau.mis.pos.application.dto.vo.PaymentVo;
import edu.scau.mis.pos.application.dto.vo.SaleVo;
import edu.scau.mis.pos.domain.entity.Payment;
import edu.scau.mis.pos.domain.entity.Sale;
import org.springframework.stereotype.Component;

/**
 * 订单转换器
 * 实现DTO与Entity的转换
 */

@Component
public class SaleAssembler {
    public SaleVo toSaleVo(Sale sale){
        SaleVo saleVo = new SaleVo();
        saleVo.setSaleNo(sale.getSaleNo());
        saleVo.setTotalAmount(sale.getTotalAmount());
        saleVo.setTotalQuantity(sale.getTotalQuantity());
        saleVo.setSaleTime(sale.getSaleTime());
        saleVo.setSaleStatus(sale.getSaleStatus().getLabel());
        return saleVo;
    }

    public PaymentVo toPaymentVo(Payment payment){
        PaymentVo paymentVo = new PaymentVo();
        paymentVo.setPaymentId(payment.getPaymentId());
        paymentVo.setPaymentSaleId(payment.getPaymentSaleId());
        paymentVo.setPaymentNo(payment.getPaymentNo());
        paymentVo.setPaymentAmount(payment.getPaymentAmount());
        paymentVo.setPaymentTime(payment.getPaymentTime());
        paymentVo.setPaymentStrategy(payment.getPaymentStrategy().getLabel());
        paymentVo.setPaymentStatus(payment.getPaymentStatus().getLabel());
        return paymentVo;
    }
}
