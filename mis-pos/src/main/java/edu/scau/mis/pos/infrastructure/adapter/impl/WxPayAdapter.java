package edu.scau.mis.pos.infrastructure.adapter.impl;

import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;
import edu.scau.mis.core.pay.wxpay.service.IWxPayService;
import edu.scau.mis.pos.infrastructure.adapter.IPayAdapter;
import edu.scau.mis.pos.infrastructure.annotation.PaymentStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@PaymentStrategy("wechat")
@Slf4j
@RequiredArgsConstructor
public class WxPayAdapter implements IPayAdapter {
    private final IWxPayService wxPayService;
    @Override
    public PayResponse pay(PayRequest payRequest) {
        log.info("===>微信native支付适配器被调用");
        return wxPayService.nativeExecute(payRequest);
    }
}
