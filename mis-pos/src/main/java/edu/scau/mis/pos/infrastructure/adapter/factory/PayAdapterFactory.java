package edu.scau.mis.pos.infrastructure.adapter.factory;

import edu.scau.mis.pos.infrastructure.adapter.IPayAdapter;
import edu.scau.mis.pos.infrastructure.adapter.impl.CashPayAdapter;
import edu.scau.mis.pos.infrastructure.annotation.PaymentStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PayAdapterFactory {
    private final List<IPayAdapter> payAdapterList;
    private final CashPayAdapter cashPayAdapter;

    /**
     * 根据payOrder支付策略获取支付处理器实例注入Spring容器
     * @param payType
     * @return
     */
    public IPayAdapter getPayStrategyAdapterHandler(String payType) {
        return payAdapterList.stream()
                .filter(item -> payType.equals(item.getClass().getAnnotation(PaymentStrategy.class).value()))
                .findFirst()
                .orElseGet(() -> cashPayAdapter);
    }
}
