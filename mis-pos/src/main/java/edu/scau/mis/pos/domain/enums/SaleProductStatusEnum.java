package edu.scau.mis.pos.domain.enums;

/**
 * 订单-产品状态枚举
 */
public enum SaleProductStatusEnum {
    ORDERED("0","已下单"),

    DELIVERED("1","已发货"),

    REFUNDED("2","已退货");

    private String value;

    private String label;

    SaleProductStatusEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    /**
     * 根据匹配value的值获取Label
     *
     * @param value
     * @return
     */
    public static String getLabelByValue(String value){
        for (SaleProductStatusEnum s : SaleProductStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s.getLabel();
            }
        }
        return "";
    }

    /**
     * 获取StatusEnum
     *
     * @param value
     * @return
     */
    public static SaleProductStatusEnum getStatusEnum(String value){
        for (SaleProductStatusEnum s : SaleProductStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s;
            }
        }
        return null;
    }
}
