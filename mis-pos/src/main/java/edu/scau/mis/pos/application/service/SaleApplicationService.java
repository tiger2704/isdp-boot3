package edu.scau.mis.pos.application.service;

import edu.scau.mis.pos.application.assembler.SaleAssembler;
import edu.scau.mis.pos.application.dto.command.EnterItemCommand;
import edu.scau.mis.pos.application.dto.command.MakePaymentCommand;
import edu.scau.mis.pos.application.dto.vo.*;
import edu.scau.mis.pos.domain.entity.Payment;
import edu.scau.mis.pos.domain.entity.Product;
import edu.scau.mis.pos.domain.entity.Sale;
import edu.scau.mis.pos.domain.entity.SaleProduct;
import edu.scau.mis.pos.domain.enums.SaleStatusEnum;
import edu.scau.mis.pos.domain.service.IProductService;
import edu.scau.mis.pos.infrastructure.factory.SaleFactory;
import edu.scau.mis.pos.domain.service.ISaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleApplicationService {
    @Autowired
    private IProductService productService;

    @Autowired
    private ISaleService saleService;

    @Autowired
    private SaleFactory saleFactory;

    @Autowired
    private SaleAssembler saleAssembler;

    private Sale currentSale; // 后期改成Redis缓存CurrentSale

    /**
     * 开始一次新销售
     * @return
     */
    public SaleVo makeNewSale(){
        SaleVo saleVo = new SaleVo();
        currentSale = saleFactory.initSale();
        // TODO：引入Redis缓存
        return saleAssembler.toSaleVo(currentSale);
    }

    /**
     * 录入商品
     * @param command
     * @return
     */
    public SaleAndProductListVo enterItem(EnterItemCommand command){
        SaleAndProductListVo saleAndProductListVo = new SaleAndProductListVo();
        Product product = productService.selectProductBySn(command.getProductSn());
        List<SaleProduct> saleProducts = currentSale.makeLineItem(product, command.getSaleQuantity());
        currentSale.getTotal();
        List<SaleProductVo> saleProductVoList = saleProducts.stream()
                .map(saleProduct -> new SaleProductVo(saleProduct.getProduct().getProductSn(), saleProduct.getProduct().getProductName(), saleProduct.getSalePrice(), saleProduct.getSaleQuantity()))
                .toList();
        saleAndProductListVo.setSaleVo(saleAssembler.toSaleVo(currentSale));
        saleAndProductListVo.setSaleProductVoList(saleProductVoList);
        return saleAndProductListVo;
    }

    /**
     * 结束销售
     * 计算优惠、持久化订单等
     * @return
     */
    public SaleVo endSale(){
        currentSale.setSaleStatus(SaleStatusEnum.SUBMITTED);
        // TODO: 持久化Sale和SaleProduct，添加事务注解
        return saleAssembler.toSaleVo(currentSale);
    }

    /**
     * 完成支付
     * @param command
     * @return
     */
    public SaleAndPaymentVo makePayment(MakePaymentCommand command){
        SaleAndPaymentVo saleAndPaymentVo = new SaleAndPaymentVo();
        // TODO: 挂单--根据saleNo获取Sale
        Payment payment = saleService.makePayment(currentSale,command.getPaymentStrategy(), command.getPaymentAmount());
        currentSale.setPayment(payment);
        currentSale.setSaleStatus(SaleStatusEnum.PAID);
        // TODO： 持久化Sale和Payment，添加事务注解
        // payment.setPaymentSaleId(sale.getSaleId());
        saleAndPaymentVo.setSaleVo(saleAssembler.toSaleVo(currentSale));
        saleAndPaymentVo.setPaymentVo(saleAssembler.toPaymentVo(payment));
        return saleAndPaymentVo;
    }
}
