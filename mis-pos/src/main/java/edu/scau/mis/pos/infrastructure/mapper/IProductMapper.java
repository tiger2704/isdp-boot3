package edu.scau.mis.pos.infrastructure.mapper;

import edu.scau.mis.pos.domain.entity.Product;

import java.util.List;

public interface IProductMapper {

    /**
     * 查询单个产品
     * @param productId 主键
     * @return 对象
     */
    Product selectProductById(Long productId);

    /**
     * 根据产品编号查询产品
     * @param productSn
     * @return
     */
    Product selectProductBySn(String productSn);

    /**
     * 查询产品列表
     * @param product 查询参数
     * @return 对象集合
     */
    List<Product> selectProductList(Product product);

    /**
     * 查询所有产品
     * @return
     */
    List<Product> selectAllProductList();

    /**
     * 新增产品
     * @param product
     * @return 影响记录数
     */
    int insertProduct(Product product);

    /**
     * 修改产品
     * @param product
     * @return 影响记录数
     */
    int updateProduct(Product product);

    /**
     * 删除产品
     * @param productId 主键
     * @return 影响记录数
     */
    boolean deleteProductById(Long productId);

}
