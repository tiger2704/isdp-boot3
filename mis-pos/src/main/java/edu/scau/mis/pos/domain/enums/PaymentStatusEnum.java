package edu.scau.mis.pos.domain.enums;

/**
 * 支付状态枚举
 */
public enum PaymentStatusEnum {
    CREATE("0","待支付"),

    PAID("1","已支付"),

    REFUNDED("2","已退款");

    private String value;

    private String label;

    PaymentStatusEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    /**
     * 根据匹配value的值获取Label
     *
     * @param value
     * @return
     */
    public static String getLabelByValue(String value){
        for (PaymentStatusEnum s : PaymentStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s.getLabel();
            }
        }
        return "";
    }

    /**
     * 获取StatusEnum
     *
     * @param value
     * @return
     */
    public static PaymentStatusEnum getStatusEnum(String value){
        for (PaymentStatusEnum s : PaymentStatusEnum.values()) {
            if(value.equals(s.getValue())){
                return s;
            }
        }
        return null;
    }
}
