package edu.scau.mis.pos.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.scau.mis.pos.domain.enums.SaleStatusEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 销售实体类
 * 聚合根
 */
@Data
public class Sale {
    private Long saleId;
    private String saleNo;
    private BigDecimal totalAmount;
    private Integer totalQuantity;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date saleTime;
    private Payment payment;
    private List<SaleProduct> saleProducts = new ArrayList<>();
    private SaleStatusEnum saleStatus;

    /**
     * 计算总金额
     * @return
     */

    public BigDecimal getTotal(){
        totalAmount = BigDecimal.ZERO;
        totalQuantity = 0;
        for(SaleProduct saleProduct: saleProducts){
            totalAmount = totalAmount.add(saleProduct.getSubTotal());
            totalQuantity = totalQuantity + saleProduct.getSaleQuantity();
        }
        return totalAmount;
    }

    /**
     * 添加订单明细
     * @param product
     * @param saleQuantity
     * @return
     */

    public List<SaleProduct> makeLineItem(Product product, Integer saleQuantity) {
        // 判断商品是否已录入，未录入则新增。已录入则修改数量。
        if(!isEntered(product.getProductSn(),saleQuantity)){
            SaleProduct saleProduct = new SaleProduct();
            saleProduct.setProduct(product);
            saleProduct.setSaleQuantity(saleQuantity);
            saleProduct.setSalePrice(product.getProductPrice());
            saleProducts.add(saleProduct);
        }
        return saleProducts;
    }

    /**
     * 判断商品是否已录入
     * 业务逻辑：如果已录入，则修改数量，否则添加saleLineItem
     * @param productSn
     * @param saleQuantity
     * @return
     */
    private boolean isEntered(String productSn, Integer saleQuantity){
        boolean flag = false;
        for(SaleProduct sp : saleProducts){
            if(productSn.equals(sp.getProduct().getProductSn())) {
                flag = true;
                Integer quantityOriginal = sp.getSaleQuantity();
                sp.setSaleQuantity(quantityOriginal + saleQuantity);
            }
        }
        return flag;
    }
}
