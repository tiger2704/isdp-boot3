package edu.scau.mis.pos.application.dto.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@AllArgsConstructor
public class SaleProductVo implements Serializable {
    private String productSn;
    private String productName;
    private BigDecimal salePrice;
    private Integer saleQuantity;

}
