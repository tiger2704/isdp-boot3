package edu.scau.mis.pos.application.dto.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class SaleAndProductListVo implements Serializable {
    private SaleVo saleVo;
    private List<SaleProductVo>  saleProductVoList;
}
