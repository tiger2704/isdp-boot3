package edu.scau.mis.pos.infrastructure.factory;

import cn.hutool.core.util.IdUtil;
import edu.scau.mis.pos.domain.entity.Sale;
import edu.scau.mis.pos.domain.enums.SaleStatusEnum;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Sale工厂类
 */
@Component
public class SaleFactory {
    public Sale initSale()
    {
        Sale sale = new Sale();
        sale.setSaleNo("so-" + IdUtil.getSnowflakeNextId());
        sale.setSaleStatus(SaleStatusEnum.CREATED);
        sale.setTotalAmount(BigDecimal.ZERO);
        sale.setTotalQuantity(0);
        sale.setSaleTime(new Date());
        return sale;
    }

}
