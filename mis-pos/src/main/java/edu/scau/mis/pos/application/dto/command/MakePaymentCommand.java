package edu.scau.mis.pos.application.dto.command;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 创建支付命令
 */
@Data
public class MakePaymentCommand implements Serializable {
    private String saleNo;
    private BigDecimal paymentAmount;
    private String paymentStrategy;
}
