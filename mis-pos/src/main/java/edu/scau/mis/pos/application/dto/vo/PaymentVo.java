package edu.scau.mis.pos.application.dto.vo;

import edu.scau.mis.pos.domain.enums.PaymentStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Data
public class PaymentVo implements Serializable {
    private Long paymentId;
    private Long paymentSaleId;
    private String paymentStrategy;
    private String paymentNo;
    private BigDecimal paymentAmount;
    private Date paymentTime;
    private String paymentStatus;
}
