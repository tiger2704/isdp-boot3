package edu.scau.mis.pos.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Product implements Serializable {
    private Long productId;

    private String productSn;

    private String productName;

    private String productDescription;

    private BigDecimal productPrice;

    private Long productCategoryId;

    private Category category;

    private String productPic;

    private String productDetailUrl;

    public Product() {
    }

    public Product(Long productId, String productSn, String productName, String productDescription, BigDecimal productPrice, Long productCategoryId, Category category, String productPic, String productDetailUrl) {
        this.productId = productId;
        this.productSn = productSn;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
        this.productCategoryId = productCategoryId;
        this.category = category;
        this.productPic = productPic;
        this.productDetailUrl = productDetailUrl;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public String getProductDetailUrl() {
        return productDetailUrl;
    }

    public void setProductDetailUrl(String productDetailUrl) {
        this.productDetailUrl = productDetailUrl;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", productPrice=" + productPrice +
                ", productCategoryId=" + productCategoryId +
                ", category=" + category +
                ", productPic='" + productPic + '\'' +
                ", productDetailUrl='" + productDetailUrl + '\'' +
                '}';
    }
}
