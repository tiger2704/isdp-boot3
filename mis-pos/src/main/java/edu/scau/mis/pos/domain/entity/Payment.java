package edu.scau.mis.pos.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.scau.mis.pos.domain.enums.PaymentStatusEnum;
import edu.scau.mis.pos.domain.enums.PaymentStrategyEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付实体类
 */
@Data
public class Payment {
    private Long paymentId;
    private Long paymentSaleId;
    private PaymentStrategyEnum paymentStrategy;
    private String paymentNo;
    private BigDecimal paymentAmount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date paymentTime;
    private PaymentStatusEnum paymentStatus;

}
