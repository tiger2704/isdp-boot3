package edu.scau.mis.pos.infrastructure.annotation;

import java.lang.annotation.*;

/**
 * 支付策略注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PaymentStrategy {
    String value() default "";
}


