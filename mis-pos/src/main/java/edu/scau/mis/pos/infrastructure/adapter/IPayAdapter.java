package edu.scau.mis.pos.infrastructure.adapter;

import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;

/**
 * 适配器
 * 统一领域稳定的接口，实现不同支付渠道的支付
 */
public interface IPayAdapter {
    PayResponse pay(PayRequest payRequest);
}
