package edu.scau.mis.pos.domain.service.impl;

import edu.scau.mis.pos.domain.entity.Category;
import edu.scau.mis.pos.infrastructure.mapper.ICategoryMapper;
import edu.scau.mis.pos.domain.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private ICategoryMapper categoryMapper;
    /**
     * 根据ID查询产品分类
     *
     * @param categoryId
     * @return
     */
    @Override
    public Category selectCategoryById(Long categoryId) {
        return categoryMapper.selectCategoryById(categoryId);
    }

    /**
     * 查询所有产品分类
     *
     * @return
     */
    @Override
    public List<Category> selectAllCategoryList() {
        return categoryMapper.selectAllCategoryList();
    }
}
