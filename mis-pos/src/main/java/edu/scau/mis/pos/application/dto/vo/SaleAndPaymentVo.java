package edu.scau.mis.pos.application.dto.vo;

import lombok.Data;

import java.io.Serializable;
@Data
public class SaleAndPaymentVo implements Serializable {
    private SaleVo saleVo;
    private PaymentVo paymentVo;

}
