package edu.scau.mis.pos.domain.service;

import edu.scau.mis.pos.domain.entity.Payment;
import edu.scau.mis.pos.domain.entity.Sale;

import java.math.BigDecimal;

public interface ISaleService {
    Payment makePayment(Sale sale, String paymentStrategy, BigDecimal paymentAmount);
}
