package edu.scau.mis.pos.infrastructure.adapter.impl;

import edu.scau.mis.core.pay.alipay.service.IAliPayService;
import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;
import edu.scau.mis.pos.infrastructure.adapter.IPayAdapter;
import edu.scau.mis.pos.infrastructure.annotation.PaymentStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@PaymentStrategy("alipay")
@Slf4j
@RequiredArgsConstructor
public class AliPayAdapter implements IPayAdapter {
    private final IAliPayService alipayService;
    @Override
    public PayResponse pay(PayRequest payRequest) {
        log.info("===> 阿里page支付适配器被调用");
        return alipayService.pageExecute(payRequest);
    }
}
