package edu.scau.mis.pos.application.dto.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Data
public class SaleVo implements Serializable {
    private String saleNo;
    private BigDecimal totalAmount;
    private Integer totalQuantity;
    private Date saleTime;
    private String saleStatus;
}
