package edu.scau.mis.pos.domain.entity;

import edu.scau.mis.pos.domain.enums.SaleProductStatusEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单-产品明细实体类
 */
@Data
public class SaleProduct {
    private Long saleProductId;

    private Long saleId;

    private Long productId;

    private Product product;

    private Integer saleQuantity;

    private BigDecimal salePrice;

    private SaleProductStatusEnum saleProductStatus;

    /**
     * 计算小计
     * @return
     */
    public BigDecimal getSubTotal() {
        return salePrice.multiply(new BigDecimal(saleQuantity));
    }
}
