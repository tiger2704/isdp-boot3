package edu.scau.mis.pos.domain.service.impl;

import edu.scau.mis.pos.domain.entity.Product;
import edu.scau.mis.pos.infrastructure.mapper.IProductMapper;
import edu.scau.mis.pos.domain.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private IProductMapper productMapper;
    /**
     * 查询单个产品
     *
     * @param productId 主键
     * @return 对象
     */
    @Override
    public Product selectProductById(Long productId) {
        return productMapper.selectProductById(productId);
    }

    /**
     * 根据产品编号查询产品
     *
     * @param productSn
     * @return
     */
    @Override
    public Product selectProductBySn(String productSn) {
        return productMapper.selectProductBySn(productSn);
    }

    /**
     * 查询产品列表
     *
     * @param product 查询参数
     * @return 对象集合
     */
    @Override
    public List<Product> selectProductList(Product product) {
        return productMapper.selectProductList(product);
    }

    /**
     * 查询所有产品
     *
     * @return
     */
    @Override
    public List<Product> selectAllProductList() {
        return productMapper.selectAllProductList();
    }

    /**
     * 新增产品
     *
     * @param product
     * @return 影响记录数
     */
    @Override
    public int insertProduct(Product product) {
        if (!checkProductSnUnique(product.getProductSn())) {
            throw new RuntimeException("产品编号已存在");
        }
        return productMapper.insertProduct(product);
    }

    /**
     * 修改产品
     *
     * @param product
     * @return 影响记录数
     */
    @Override
    public int updateProduct(Product product) {
        return productMapper.updateProduct(product);
    }

    /**
     * 删除产品
     *
     * @param productId 主键
     * @return 影响记录数
     */
    @Override
    public boolean deleteProductById(Long productId) {
        return productMapper.deleteProductById(productId);
    }

    /**
     * 检查产品编号是否唯一
     * @param productSn
     * @return
     */
    private boolean checkProductSnUnique(String productSn) {
        return productMapper.selectProductBySn(productSn) == null;
    }
}
