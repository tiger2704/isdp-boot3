package edu.scau.mis.pos.domain.enums;

/**
 * 支付策略枚举
 */
public enum PaymentStrategyEnum {
    WECHAT("wechat","微信支付"),

    ALIPAY("alipay","支付宝"),

    CASH("cash","现金");

    private String value;

    private String label;

    PaymentStrategyEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    /**
     * 根据匹配value的值获取Label
     *
     * @param value
     * @return
     */
    public static String getLabelByValue(String value){
        for (PaymentStrategyEnum s : PaymentStrategyEnum.values()) {
            if(value.equals(s.getValue())){
                return s.getLabel();
            }
        }
        return "";
    }

    /**
     * 获取StatusEnum
     *
     * @param value
     * @return
     */
    public static PaymentStrategyEnum getStrategyEnum(String value){
        for (PaymentStrategyEnum s : PaymentStrategyEnum.values()) {
            if(value.equals(s.getValue())){
                return s;
            }
        }
        return null;
    }
}
