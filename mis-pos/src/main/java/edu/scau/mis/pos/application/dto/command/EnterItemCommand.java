package edu.scau.mis.pos.application.dto.command;

import lombok.Data;

import java.io.Serializable;

/**
 * 输入订单明细命令
 */
@Data
public class EnterItemCommand implements Serializable {
    private String saleNo;

    private String productSn;

    private Integer saleQuantity;
}
