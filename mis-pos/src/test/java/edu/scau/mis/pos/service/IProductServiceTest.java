package edu.scau.mis.pos.service;

import edu.scau.mis.pos.domain.entity.Product;
import edu.scau.mis.pos.domain.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
class IProductServiceTest {
    @Autowired
    private IProductService productService;

    @org.junit.jupiter.api.Test
    void selectProductById() {
        Product product = productService.selectProductById(1L);
        System.out.println(product);

    }

    @org.junit.jupiter.api.Test
    void selectProductBySn() {
        Product product = productService.selectProductBySn("1001");
        System.out.println(product);
    }

    @org.junit.jupiter.api.Test
    void selectProductList() {
        Product product = new Product();
        product.setProductName("笔");
        List<Product> list = productService.selectProductList(product);
        list.forEach(System.out::println);
    }

    @org.junit.jupiter.api.Test
    void selectAllProductList() {
        List<Product> list = productService.selectAllProductList();
        list.forEach(System.out::println);
    }

    @org.junit.jupiter.api.Test
    void insertProduct() {
        Product product = new Product();
        product.setProductSn("1011");
        product.setProductName("测试");
        product.setProductDescription("这是测试例子");
        product.setProductPrice(new BigDecimal("10.00"));
        product.setProductCategoryId(3L);
        productService.insertProduct(product); // productId = 11L
        Product p = productService.selectProductById(11L);
        System.out.println(p);
    }

    @org.junit.jupiter.api.Test
    void updateProduct() {
        Product product = productService.selectProductBySn("1001");
        product.setProductName("测试修改");
        productService.updateProduct(product);
        System.out.println(productService.selectProductBySn("1001"));
    }

    @org.junit.jupiter.api.Test
    void deleteProductById() {
        productService.deleteProductById(1L);
        System.out.println(productService.selectAllProductList().size());
    }
}