package edu.scau.mis.web.controller;

import edu.scau.mis.pos.application.dto.command.EnterItemCommand;
import edu.scau.mis.pos.application.dto.command.MakePaymentCommand;
import edu.scau.mis.pos.application.dto.vo.SaleAndPaymentVo;
import edu.scau.mis.pos.application.dto.vo.SaleAndProductListVo;
import edu.scau.mis.pos.application.dto.vo.SaleVo;
import edu.scau.mis.pos.application.service.SaleApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sale")
public class SaleController {
    @Autowired
    private SaleApplicationService saleApplicationService;
    @GetMapping("/makeNewSale")
    public ResponseEntity<SaleVo> makeNewSale()
    {
        return ResponseEntity.ok(saleApplicationService.makeNewSale());
    }

    @PostMapping("/enterItem")
    public ResponseEntity<SaleAndProductListVo> enterItem(@RequestBody  EnterItemCommand enterItemCommand)
    {
        return ResponseEntity.ok(saleApplicationService.enterItem(enterItemCommand));
    }

    @GetMapping("/endSale")
    public ResponseEntity<SaleVo> endSale()
    {
        return ResponseEntity.ok(saleApplicationService.endSale());
    }

    @PostMapping("/makePayment")
    public ResponseEntity<SaleAndPaymentVo> makePayment(@RequestBody MakePaymentCommand makePaymentCommand)
    {
        return ResponseEntity.ok(saleApplicationService.makePayment(makePaymentCommand));
    }
}
