package edu.scau.mis.web.controller;

import edu.scau.mis.pos.domain.entity.Category;
import edu.scau.mis.pos.domain.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;

    @GetMapping("/{categoryId}")
    public ResponseEntity<Category> getById(@PathVariable("categoryId")  Long categoryId) {
        return ResponseEntity.ok(categoryService.selectCategoryById(categoryId));
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Category>> listAll(){
        return ResponseEntity.ok(categoryService.selectAllCategoryList());
    }
}
