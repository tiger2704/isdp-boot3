package edu.scau.mis.web.controller;

import edu.scau.mis.pos.domain.entity.Product;
import edu.scau.mis.pos.domain.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    @Autowired
    private IProductService productService;
    @GetMapping("/{productId}")
    public ResponseEntity<Product> getById(@PathVariable("productId")  Long productId)
    {
        return ResponseEntity.ok(productService.selectProductById(productId));
    }

    @GetMapping("/getBySn")
    public ResponseEntity<Product> getBySn(@RequestParam("productSn") String productSn )
    {
        return ResponseEntity.ok(productService.selectProductBySn(productSn));
    }

    @GetMapping("/list")
    public ResponseEntity<List<Product>> list(Product product){
        return ResponseEntity.ok(productService.selectProductList(product));
    }

    @GetMapping("/listAll")
    public ResponseEntity<List<Product>> listAll(){
        return ResponseEntity.ok(productService.selectAllProductList());

    }
    @PostMapping("/add")
    public ResponseEntity<Integer> add(@RequestBody Product product){
        return ResponseEntity.ok(productService.insertProduct(product));
    }

    @PutMapping("/edit")
    public ResponseEntity<Integer> edit(@RequestBody Product product){
        return ResponseEntity.ok(productService.updateProduct(product));
    }

    @DeleteMapping("/remove/{productId}")
    public ResponseEntity<Boolean> remove(@PathVariable("productId") Long productId){
        return ResponseEntity.ok(productService.deleteProductById(productId));
    }
}
