package edu.scau.mis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsdpApplication {
    public static void main(String[] args) {
        SpringApplication.run(IsdpApplication.class, args);
        System.out.println("IsdpApplication启动成功");
    }
}