package edu.scau.mis.core.pay.wxpay.service;

import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;

public interface IWxPayService {
    PayResponse nativeExecute(PayRequest payRequest);
}
