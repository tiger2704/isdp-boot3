package edu.scau.mis.core.pay.wxpay.service.impl;

import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.service.payments.nativepay.NativePayService;
import com.wechat.pay.java.service.payments.nativepay.model.Amount;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayRequest;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayResponse;
import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;
import edu.scau.mis.core.pay.wxpay.config.WxPayConfig;
import edu.scau.mis.core.pay.wxpay.service.IWxPayService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class WxPayServiceImpl implements IWxPayService {

    private final WxPayConfig wxPayConfig;
    private final Config config;

    @Override
    public PayResponse nativeExecute(PayRequest payRequest) {
        try {
            log.info("===>微信native支付被调用");
            NativePayService service = new NativePayService.Builder().config(config).build();

            PrepayRequest request = new PrepayRequest();
            request.setAppid(wxPayConfig.getAppid());
            request.setMchid(wxPayConfig.getMerchantId());
            request.setNotifyUrl(wxPayConfig.getNotifyDomain());

            request.setOutTradeNo(payRequest.getTradeNo());
            Amount amount = new Amount();
            // BigDecimal单位元 转换 Integer单位分
            amount.setTotal(payRequest.getTotalAmount().intValue()*100);
            request.setAmount(amount);
            request.setDescription(payRequest.getSubjectDescription());
            log.info("WxPay请求参数: {}", request);

            PrepayResponse prepayResponse = service.prepay(request);
            log.info("WxPay native支付成功，返回结果: {}", prepayResponse);

            log.info("===> 微信支付二维码url {}", prepayResponse.getCodeUrl());

            PayResponse payResponse = new PayResponse();
            payResponse.setTradeNo(payRequest.getTradeNo());
            payResponse.setCodeUrl(prepayResponse.getCodeUrl());
            return payResponse;
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("创建WxPay支付交易失败");
        }
    }
}

