package edu.scau.mis.core.pay.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class PayResponse implements Serializable {
    private String tradeNo;

    private String responseBody;

    private String codeUrl;

}
