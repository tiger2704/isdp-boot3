package edu.scau.mis.core.pay.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PayRequest implements Serializable {
    private String tradeNo;
    private BigDecimal totalAmount;
    private String subjectDescription;
}
