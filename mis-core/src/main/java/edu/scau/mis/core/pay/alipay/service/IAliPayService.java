package edu.scau.mis.core.pay.alipay.service;

import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;

public interface IAliPayService {
    PayResponse pageExecute(PayRequest payRequest);
}
