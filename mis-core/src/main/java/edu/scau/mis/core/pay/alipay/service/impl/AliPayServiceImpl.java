package edu.scau.mis.core.pay.alipay.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import edu.scau.mis.core.pay.alipay.config.AliPayConfig;
import edu.scau.mis.core.pay.alipay.service.IAliPayService;
import edu.scau.mis.core.pay.domain.PayRequest;
import edu.scau.mis.core.pay.domain.PayResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AliPayServiceImpl implements IAliPayService {
    private final AliPayConfig alipayClientConfig;
    private final AlipayClient alipayClient;
    @Override
    public PayResponse pageExecute(PayRequest payRequest) {
        try {
            log.info("调用AliPay 支付接口");
            // 网页支付
            // AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
            AlipayTradePrecreateRequest alipayRequest = new AlipayTradePrecreateRequest();
            alipayRequest.setNotifyUrl(alipayClientConfig.getReturnUrl());
            alipayRequest.setReturnUrl(alipayClientConfig.getReturnUrl());

            JSONObject bizContent = new JSONObject();
            bizContent.put("out_trade_no", payRequest.getTradeNo());
            bizContent.put("total_amount", payRequest.getTotalAmount());
            bizContent.put("subject", payRequest.getSubjectDescription());
            log.info("AliPay请求参数: {}", bizContent);

            alipayRequest.setBizContent(bizContent.toString());
            // 网页支付
//            AlipayTradePagePayResponse alipayResponse = alipayClient.pageExecute(alipayRequest);
            AlipayTradePrecreateResponse alipayResponse = alipayClient.execute(alipayRequest);

            if(alipayResponse.isSuccess()){
                log.info("AliPay page支付成功，返回结果 ===> " + alipayResponse.getBody());
                PayResponse payResponse = new PayResponse();
                payResponse.setResponseBody(alipayResponse.getBody());
                payResponse.setTradeNo(payRequest.getTradeNo());
                payResponse.setCodeUrl(alipayResponse.getQrCode());
                log.info("===> 阿里支付二维码url {}", payResponse.getCodeUrl());

                return payResponse;
            } else {
                log.info("AliPay 支付失败，返回码 ===> " + alipayResponse.getCode() + ", 返回描述 ===> " + alipayResponse.getMsg());
                throw new RuntimeException("创建AliPay支付交易失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("创建AliPay支付交易失败");
        }
    }
}

